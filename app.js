const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const express = require("express");
const cors = require("cors");
const mysql = require("mysql2");

// Connect to Database
const con = mongoose.connect("mongodb://5.189.177.98:37017/SampleUsers", { useNewUrlParser: true });

global.mycon = mysql.createConnection({
    host: '5.189.177.98',
    user: 'sample_user',
    password: 'qwerty12!@',
    database: 'sample_db'
});

// Initialize Express Application
const app = express();

// Listen to port 3000
app.listen(3000);


// Define Database Models
global.User = require('./app/models/User');


// Middlewares
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Routes
app.get("/", (req, res) => {
    res.send("Hello world !!");
});

app.use("/users", require("./app/routes/users"));
app.use("/search", require("./app/routes/search"));