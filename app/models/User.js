const mongoose = require("mongoose");
const mongoosastic = require('mongoosastic');

const userSchema = mongoose.Schema({
    firstName: String,
    lastName: String,
    username: String,
    password: String,
    status: Number,
    gender: String,
    createdAt: Date
});

userSchema.plugin(mongoosastic, {
    hosts: ["5.189.177.98:39200"]
});

module.exports = mongoose.model('User', userSchema);