const express = require('express');
const router = express.Router();
const SearchController = require('../controllers/SearchController');


router.get("/mysql/:key?", SearchController.getSearchMysql);
router.get("/mongo/:key?", SearchController.getSearchMongo);
router.get("/elastic/:key?", SearchController.getSearchElastic);


module.exports = router;