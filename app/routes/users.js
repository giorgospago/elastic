const express = require('express');
const router = express.Router();
const UsersController = require('../controllers/UsersController');

router.get("/migrate", UsersController.findUsersFromSql);

router.get("/es/:key?", UsersController.getUserListES);

router.get("/:key?", UsersController.getUserList);
router.post("/create", UsersController.postUserCreate);
router.delete("/delete", UsersController.deleteUser);
router.put("/update", UsersController.putUpdateUser);

module.exports = router;