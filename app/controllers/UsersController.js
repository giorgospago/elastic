const _ = require("lodash");

const getUserListES = (req, res) => {
    const key = req.params.key;

    User.search(
        {
            query_string: {
                query: "*" + key + "*",
                fields: [ "firstName", "lastName" ]
            }
        },
        (err, results) => {
            if (err) {
                return res.json(err);
            }
            res.json(results.hits.hits.map(item => ({
                _id: item._id,
                firstName: item._source.firstName,
                lastName: item._source.lastName,
                gender: item._source.gender
            })));
        }
    );
};

const getUserList = async (req, res) => {
    try {
        let filters = {};
        const key = req.params.key;
        if (key) {
            filters = {
                $or: [
                    {"firstName": {$regex: key, $options: "i"}},
                    {"lastName": {$regex: key, $options: "i"}}
                ]
            };
        }
        const users = await User.find(filters).exec();
        res.json(users);
    } catch(e) {
        res.json(e);
    }
};

const postUserCreate = (req, res) => {
    const first_name = req.body.first_name;
    const last_name = req.body.last_name;
    const email = req.body.email;
    const age = req.body.age;

    const u = new User({
        first_name: first_name,
        last_name: last_name,
        email: email,
        age: age
    });

    u.save();

    res.send("User Created");
};

const deleteUser = async (req, res) => {
    const user_id = req.body.user_id;
    const deletedUser = await User.findByIdAndRemove(user_id).exec();

    if (!deletedUser) {
        return res.json({msg: "User not found"});
    }

    res.json({msg: deletedUser.first_name + " Deleted successfully"});
};

const putUpdateUser = async (req, res) => {
    const user_id = req.body.user_id;

    const fields = [
        "first_name",
        "last_name",
        "email",
        "age"
    ];

    const updateObj = {};
    for(field of fields) {
        if(req.body[field]) {
            updateObj[field] = req.body[field];
        }
    }

    const updatedUser = await User.findByIdAndUpdate(user_id, updateObj).exec();
    res.json({msg: "User Updates successfully", updatedUser});
};

const findUsersFromSql = (req, res) => {
    mycon.query("select * from user_details limit 10000", async (err, results) => {
        if(err) {
            return res.json(err);
        }

        const start = Date.now();

        const converted = [];
        for (result of results) {
            const mongoUser = {
                firstName: result.first_name,
                lastName: result.last_name,
                username: result.username,
                password: result.password,
                status: result.status,
                gender: result.gender,
                createdAt: result.created_at
            };
            converted.push(mongoUser);
        }

        const chunkSize = 100;
        const chunks = _.chunk(converted, chunkSize);

        for (let chunk of chunks) {
            await User.insertMany(chunk);
        }

        res.send("Elapsed: " + (Date.now() - start) + "ms");
    });
};

const deleteAllUsersFromMongo = () => {
    // User.remove({}).exec(() => {
    //     console.log("ok");
    // });
    //
    // User.esTruncate(function(err){
    //     console.log("esTruncated");
    // });

    // User.synchronize();
};

// deleteAllUsersFromMongo();

module.exports = {
    getUserList,
    postUserCreate,
    deleteUser,
    putUpdateUser,
    findUsersFromSql,
    getUserListES
};
