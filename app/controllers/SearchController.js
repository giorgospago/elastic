const getSearchMysql = (req, res) => {
    const key = req.params.key;
    const query = `
        SELECT
            user_id as _id,
            first_name as firstName,
            last_name as lastName,
            gender
        FROM user_details
        WHERE
            first_name LIKE "%${key}%" OR
            last_name LIKE "%${key}%"
        LIMIT 10
    `;
    mycon.query(query, async (err, results) => {
        if(err) {
            return res.json(err);
        }

        res.json(results);
    });
};

const getSearchMongo = async (req, res) => {
    try {
        let filters = {};
        const key = req.params.key;
        if (key) {
            filters = {
                $or: [
                    {"firstName": {$regex: key, $options: "i"}},
                    {"lastName": {$regex: key, $options: "i"}}
                ]
            };
        }
        const users = await User.find(filters)
            .select("firstName lastName gender")
            .limit(10)
            .exec();
        res.json(users);
    } catch(e) {
        res.json(e);
    }
};

const getSearchElastic = (req, res) => {
    const key = req.params.key;

    User.search(
        {
            query_string: {
                query: "*" + key + "*",
                fields: [ "firstName", "lastName" ]
            }
        },
        (err, results) => {
            if (err) {
                return res.json(err);
            }
            res.json(results.hits.hits.map(item => ({
                _id: item._id,
                firstName: item._source.firstName,
                lastName: item._source.lastName,
                gender: item._source.gender
            })));
        }
    );
};

module.exports = {
    getSearchMysql,
    getSearchMongo,
    getSearchElastic
};
